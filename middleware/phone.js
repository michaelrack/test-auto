import { useAccountStore } from "~~/store/account";

export default defineNuxtRouteMiddleware((to, from) => {
  const account = useAccountStore();

  if (!account.user?.phone) {
    // toast.add({
    //   severity: "error",
    //   detail: "You are not logged in",
    //   summary: "This page is not accessible",
    //   life: 3000,
    // });
    return navigateTo("/login");
  }
});
