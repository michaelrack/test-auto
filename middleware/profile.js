import { useAccountStore } from "~~/store/account";
import { getCookie } from "~~/utils/utilsCookies";

export default defineNuxtRouteMiddleware((to, from) => {
  const account = useAccountStore();
  const token = getCookie("Bearer-token");

  if (token) {
    account.SET_AUTH(true);
  }

  if (account.isAuth) {
    return navigateTo("/projects");
  }
});
