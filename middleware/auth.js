import { useAccountStore } from "~~/store/account";
import { getCookie } from "~~/utils/utilsCookies";

export default defineNuxtRouteMiddleware((to, from) => {
  const account = useAccountStore();
  const token = getCookie("Bearer-token");

  if (token) {
    account.SET_AUTH(true);
  }

  if (!account.isAuth) {
    // toast.add({
    //   severity: "error",
    //   detail: "You are not logged in",
    //   summary: "This page is not accessible",
    //   life: 3000,
    // });
    return navigateTo("/");
  }
});
