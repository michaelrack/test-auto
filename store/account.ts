import { defineStore } from "pinia";

export const useAccountStore = defineStore("account", {
  state: () => {
    return { user: null, isAuth: false };
  },
  actions: {
    SET_USER(data: any) {
      this.user = data;
    },
    SET_AUTH(data: boolean) {
      this.isAuth = data;
    },
  },
});
