import PrimeVue from "primevue/config";
import Button from "primevue/button";
import InputText from "primevue/inputtext";
import Toast from "primevue/toast";
import ToastService from "primevue/toastservice";
import InputNumber from "primevue/inputnumber";
import Sidebar from "primevue/sidebar";
import InputMask from "primevue/inputmask";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(PrimeVue, { ripple: true });
  nuxtApp.vueApp.use(ToastService);
  nuxtApp.vueApp.component("Button", Button);
  nuxtApp.vueApp.component("InputText", InputText);
  nuxtApp.vueApp.component("InputNumber", InputNumber);
  nuxtApp.vueApp.component("InputMask", InputMask);
  nuxtApp.vueApp.component("Toast", Toast);
  nuxtApp.vueApp.component("Sidebar", Sidebar);
  //other components that you need
});
