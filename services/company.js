import { accountApi } from "./axios";

export const getCompanies = () => accountApi("/v1/companies", { method: "GET" });

export const getCompany = (id) => accountApi(`/v1/companies/${id}`, { method: "GET" });
