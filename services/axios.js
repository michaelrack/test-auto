import axios from "axios";

export const api = ((options = { headers: {} }) => {
  const { $config } = useNuxtApp();
  const axiosInstance = axios.create({
    baseURL: $config.API_URL,
    ...options,
    header: {
      "Access-Control-Allow-Origin": "*",
      ...options.headers,
    },
  });

  axiosInstance.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      console.warn("Interceptor called", error);
      return Promise.reject(error);
    }
  );

  return axiosInstance;
})();

export const accountApi = async (path, options) => {
  const { $config } = useNuxtApp();
  const BearerToken = useCookie("Bearer-token").value;
  const request = $fetch(`${$config.API_URL}${path}`, {
    ...options,
    headers: {
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
      Authorization: `Bearer ${BearerToken}`,
      ...options.headers,
    },
  });
  const response = await request;
  return response;
};
