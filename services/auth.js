import { accountApi, api } from "./axios";

export const register = (data) => api.post("/v1/auth/register", data);

export const login = (data) => api.post("/v1/auth/login", data);

export const loginWithSms = (data) => api.post("/v1/auth/sms-login", data);

export const passwordResetLink = (data) => api.post("/v1/auth/reset-password-request", data);

export const passwordReset = (data) => api.post("/v1/auth/reset-password", data);

export const me = () => accountApi("/v1/auth/me", { method: "GET" });
