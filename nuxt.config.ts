export default defineNuxtConfig({
  ssr: false,
  css: [
    "primevue/resources/themes/saga-blue/theme.css",
    "primevue/resources/primevue.css",
    "primeicons/primeicons.css",
    "primeflex/primeflex.css",
  ],
  build: {
    transpile: ["primevue"],
  },
  app: {
    head: {
      title: "InFlow Test",
    },
  },
  modules: ["@pinia/nuxt"],
  plugins: ["~/plugins/primevue.js"],
  runtimeConfig: {
    public: {
      API_URL: process.env.API_URL,
    },
  },
});
